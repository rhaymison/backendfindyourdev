<?php

use App\Endereco;
use App\User;
use Illuminate\Database\Seeder;

class UserTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createMyProfile();
    }

    public function createMyProfile()
    {
        $user = new User;
        $user->name = 'Rhaymison Cristian Heleno O. Betini';
        $user->email = 'rhaymiso@hotmail.com';
        $user->password = \Hash::make('123456');
        $user->git = 'Dev-Rhay-Betini';
        $user->avatar = '"https://avatars3.githubusercontent.com/u/55612362?v=4"';
        $user->url = '"https://api.github.com/users/Dev-Rhay-Betini"';
        $user->seguidores = 1;
        $user->seguindo = 3;
        $user->save();

        $address = new Endereco;
        $address->cep = '29140-669';
        $address->rua = 'Avenida Fernando Antonio';
        $address->numero = '214';
        $address->bairro = 'Sotelandia';
        $address->cidade  = 'Cariacica';
        $address->estado = 'ES';
        $address->user_id = $user->id;
        $address->save();
    }
}
