<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function getUserByGitName($id)
    {
        try {
            $user = UserRepository::getUserByGitName($id);
            return response()->json($user, 200);
        } catch (\Exception $e) {
            \Log::alert($e->getMessage());
            return response()->json(['errro' => $e->getMessage(), 405]);
        }
    }
}
