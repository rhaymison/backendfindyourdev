<?php

namespace App\Repositories;

use App\User;

class UserRepository
{
    public static function getUserByGitName($gitName)
    {
        $user = User::where('git', $gitName)->first();
        if ($user) $user->load('endereco');
        return $user;
    }
}
